import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
});

imageCategory = new Meteor.Collection("image");
messageChat = new Meteor.Collection("message");

Meteor.methods({
    'insertCategory': function(_image, _code){
        console.log("Hello world");
        imageCategory.insert({image:_image, code: _code});
    }
});
Meteor.methods({
    functionToBeCalled(aName){
        console.log(aName);
        return imageCategory.find({code: aName}).fetch()[0].image;
        //return "aaa";
    }
});
Meteor.methods({
    'insertMessage': function(_user, _userRelative, _content, _time){
        console.log("Hello world");
        messageChat.insert({user:_user, userRelative:_userRelative, content: _content, time: _time});
    }
});
Meteor.methods({
    getMessages(random){
        console.log(random);
        return messageChat.find({userRelative: random}, {limit:10}).fetch();
        //return "aaa";
    }
});

