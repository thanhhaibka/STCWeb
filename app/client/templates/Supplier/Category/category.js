//import "../../client/templates/Supplier/category.html";
import {Meteor} from "meteor/meteor";

categoryImage = new Meteor.Collection("image");

var image, imgSrc;
if(Meteor.isClient) {

    Template.uploadForm.events({
        'change #fileInput': function (e, template) {
            if (e.currentTarget.files && e.currentTarget.files[0]) {
                // We upload only one file, in case
                // there was multiple files selected
                var file = e.currentTarget.files[0];
                //console.log("file:" + file);
                var FR = new FileReader();
                //console.log("fr: " + FR);
                FR.addEventListener("load", function (ev) {
                    // console.log("image: " + ev.target.result);
                    image = ev.target.result;
                });
                // console.log("image: " + image);
                FR.readAsDataURL(file);
            }
        }
    });

    Template.Category.events({
        'click #addCategory': function (e) {
            e.preventDefault();
            Meteor.call("insertCategory", image.toString(), $('#code').val().toString());
        }
    });
    Template.Category.rendered = function() {
        Meteor.call('functionToBeCalled', "2347867586324", function(err, response) {
            //console.log(err + "asdsd" + response);
            imgSrc = response;
            var newImage = document.createElement('img');
            newImage.src = imgSrc;
            //
            console.log(imgSrc);

            document.getElementById("imgTest").innerHTML = newImage.outerHTML;
        });
        // var imgSrc = categoryImage.find({code:'2347867586324'}).fetch(function(err, result){
        //     console.log(err);
        //     console.log(result);
        // });


        // console.log(Router.current().params.query.id);
        // $('#code').val(Router.current().params.query.id);
    }
}

if(Meteor.isServer) {

    Meteor.startup(function() {
        return category.find({});
    });
    Meteor.publish("category", function(){
        observerSubscription(this, "category", function() {
            return category.find();
        });
    });
    category.allow( {
        insert() {
            /*here goes the logic to determine if someone is allowed*/
            return true;
        },
        update() { return true; },
        remove() { return true; }
    } )
}
