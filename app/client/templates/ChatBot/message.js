
import './message.html';
import {Template} from 'meteor/templating';
import './text';
import {Meteor} from "meteor/meteor";

Template.message.helpers({
    messages: function () {
        return Session.get('messages')
    }
});

Template.message.events({
    'submit .new-message': function (e) {
        e.preventDefault();
        Meteor.call("insertMessage", Session.get("random"), Session.get("random"), $('#content').val(), new Date().toLocaleTimeString());
        $('#content').val('');
        Meteor.call("insertMessage", "system", Session.get("random"), "Xin chào!", new Date().toLocaleTimeString());
        Meteor.call('getMessages',  Session.get("random"), function(err, response) {
            console.log(response);
            Session.set("messages", response);
        });

    }
});

Template.message.rendered = function() {

    Session.set("random", Math.floor((Math.random() * 100000) + 1));
    Meteor.call("insertMessage", "system", Session.get("random"), "Tôi có thể giúp gì cho bạn!", new Date().toLocaleTimeString());
    Meteor.call('getMessages', Session.get("random"), function(err, response) {
        console.log(response);
        Session.set("messages", response);
    });
};