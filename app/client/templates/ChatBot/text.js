import { Template } from 'meteor/templating';
import './text.html';

Template.text.helpers({
    equals: function(a, b) {
        return a === b;
    },
});