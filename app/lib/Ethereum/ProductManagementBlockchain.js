// Constructor
function ProductManagementBlockchain(_productAbi, _productByteCode = null, _productAddress = null) {
    // always initialize all instance properties
    this.productAbi = _productAbi;
    this.productAddress = _productAddress; 
    this.productByteCode = _productByteCode;
    this.ProductContract = new web3.eth.Contract(productAbi);
}
// get product information from blockchain
ProductManagementBlockchain.prototype.getProductInfo = function(address) {
    
};
  // export the class
module.exports = ProductManagementBlockchain;