pragma solidity ^0.4.2;
import './lib/Ownable.sol';

contract Product is Ownable {
    string name;
    bytes32 code;    //include supplier code + category code
    uint8 rating;
    string comment;
    bool isCheck;
    bool isRate;
    string image;

    event Rate(address from, uint256 value);
    event CheckCode(address from, uint256 value);

    constructor(string _name, bytes32 _code, string _image) public {
        name = _name;
        code = _code;
        owner = msg.sender;
        image = _image;
        isCheck = false;
        isRate = false;
    }

    function getInfo() public view returns(string, uint8, string, bool, bool, string){
        return(name, rating, comment, isCheck, isRate, image);
    }

    function rate(uint8 _rating, string _comment, bytes32 _code) payable public returns (bool){
        require(isRate == false);
        require(sha256(_code) == code);
        require(msg.value > 0);
        rating = _rating;
        comment = _comment;
        isRate = true;
        emit Rate(msg.sender, msg.value);
        return true;
    }

    function checkCode() payable public returns (bool){
        require(isCheck == false);
        isCheck = true;
        emit CheckCode(msg.sender, msg.value);
        return false;
    }
}
