pragma solidity ^0.4.2;
import './lib/Ownable.sol';
import './ProductCategory.sol';

contract Supplier is Ownable {
    string name;
    string code;    //include supplier code + category code
    int256 sumRating;
    int256 numOfRating;
    string comment;
    string location;

    ProductCategory[] lstCategory;

    constructor(string _name, string _code, string _location) public {
        name = _name;
        code = _code;
        location = _location;
        owner = msg.sender;
    }

    function getInfo() public view returns(string, string, int256, int256, string, string){
        return(name, code, sumRating, numOfRating, comment, location);
    }

    function addCategory(string _name, string _code, string _image) public onlyOwner{
        ProductCategory newCategory = new ProductCategory(_name, _code, _image);
        lstCategory.push(newCategory);
    }
}