pragma solidity ^0.4.2;
import './lib/Ownable.sol';
import './Product.sol';

contract ProductCategory is Ownable {
    string name;
    string code;        //include supplier code
    int256 sumRating;
    int256 numOfRating;
    string image;

    Product[] lstProduct;

    constructor(string _name, string _code, string _image) public {
        name = _name;
        code = _code;
        image = _image;
        owner = msg.sender;
    }

    function getInfo() public view returns(string, string, int256, int256, string){
        return(name, code, sumRating, numOfRating, image);
    }

    function addProduct(string _name, string _code, string _image) public onlyOwner {
        Product newProduct = new Product(_name, _code, _image);
        lstProduct.push(newProduct);
    }

}